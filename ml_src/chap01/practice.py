def ret_info_myself(name1: str, name2: str, age: str) -> str:
    if type(age) == int:
        age = str(age)
    return "私は{0}{1}です。{2}才です。".format(name1, name2, age)


def main():
    name1, name2, age = input().split()
    result = ret_info_myself(name1, name2, age)
    print(result)


if __name__ == "__main__":
    main()
