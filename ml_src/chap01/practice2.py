class Hello(object):
    def ret_hellos(self, name: str) -> str:
        if type(name) != str:
            raise ValueError
        return "{}さん、こんにちは".format(name)


def main():
    names = ["田中", "佐藤", "山田", "高橋"]
    for name in names:
        print(Hello.ret_hellos(name))


if __name__ == "__main__":
    main()
